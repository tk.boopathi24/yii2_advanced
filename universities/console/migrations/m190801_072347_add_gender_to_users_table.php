<?php

use yii\db\Schema;
use yii\db\Migration;

class m190801_072347_add_gender_to_users_table extends Migration
{
    public function up()
    {
       $this->addColumn('user', 'gender', "ENUM('male','female')");
       $this->addColumn('user', 'hobbies', Schema::TYPE_TEXT);
        $this->addColumn('user', 'user_status', Schema::TYPE_BOOLEAN);
        
    }

    public function down()
    {

        $this->dropColumn('user', 'gender');
        $this->dropColumn('user', 'user_status');
        $this->dropColumn('user', 'hobbies');

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
