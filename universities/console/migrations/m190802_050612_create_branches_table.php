<?php

use yii\db\Schema;
use yii\db\Migration;

class m190802_050612_create_branches_table extends Migration
{
    public function safeUp()
    {

      $this->createTable('branches',[
        'id' => Schema::TYPE_PK,
        'college_id' => Schema::TYPE_INTEGER."  NOT NULL",
        'name' => Schema::TYPE_STRING ."  NOT NULL",
        'address' => Schema::TYPE_STRING,
        'status' => "ENUM('active','inactive')",
        'created_by' => Schema::TYPE_INTEGER."  NOT NULL",
        'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
        'updated_at' => Schema::TYPE_DATETIME,
    ]);

    $this->createIndex(
        'idx-branches-created_by',
        'branches',
        'created_by'
    );

    $this->addForeignKey(
        'fk-branches-created_by',
        'branches',
        'created_by',
        'user',
        'id',
        'CASCADE'
    );

    $this->createIndex(
      'idx-branches-college_id',
      'branches',
      'college_id'
  );
  
  $this->addForeignKey(
      'fk-branches-college_id',
      'branches',
      'college_id',
      'colleges',
      'id',
      'CASCADE'
  );    



    }

    public function safeDown()
    {
      $this->dropForeignKey(
        'fk-branches-created_by',
        'branches'
    );

    $this->dropIndex(
        'idx-branches-created_by',
        'branches'
    );
    $this->dropForeignKey(
      'fk-branches-college_id',
      'branches'
  );

  $this->dropIndex(
      'idx-branches-college_id',
      'branches'
  );    

    $this->dropTable('branches');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
