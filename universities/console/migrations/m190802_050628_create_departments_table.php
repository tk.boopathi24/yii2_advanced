<?php

use yii\db\Schema;
use yii\db\Migration;

class m190802_050628_create_departments_table extends Migration
{
    public function safeUp()
    {
      $this->createTable('departments',[
        'id' => Schema::TYPE_PK,
        'college_id' => Schema::TYPE_INTEGER."  NOT NULL",
        'branch_id' => Schema::TYPE_INTEGER."  NOT NULL",
        'name' => Schema::TYPE_STRING ."  NOT NULL",
        'strength' => Schema::TYPE_INTEGER,
        'status' => "ENUM('active','inactive')",
        'created_by' => Schema::TYPE_INTEGER."  NOT NULL",
        'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
        'updated_at' => Schema::TYPE_DATETIME,
    ]);
    $this->createIndex(
      'idx-departments-created_by',
      'departments',
      'created_by'
  );

  $this->addForeignKey(
      'fk-departments-created_by',
      'departments',
      'created_by',
      'user',
      'id',
      'CASCADE'
  );

  $this->createIndex(
    'idx-departments-college_id',
    'departments',
    'college_id'
);

$this->addForeignKey(
    'fk-departments-college_id',
    'departments',
    'college_id',
    'colleges',
    'id',
    'CASCADE'
);    

 }

    public function safeDown()
    {
      $this->dropForeignKey(
        'fk-departments-created_by',
        'departments'
    );

    $this->dropIndex(
        'idx-departments-created_by',
        'departments'
    );
    $this->dropForeignKey(
      'fk-departments-college_id',
      'departments'
  );

  $this->dropIndex(
      'idx-departments-college_id',
      'departments'
  );    

    $this->dropTable('departments');

        
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
