<?php

use yii\db\Schema;
use yii\db\Migration;

class m190807_053156_add_logo_column_to_colleges_table extends Migration
{
    public function up()
    {
        $this->addColumn('colleges', 'logo', 'VARCHAR(255) AFTER name');
    }

    public function down()
    {
        $this->dropColumn('colleges','logo');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
