<?php

use yii\db\Schema;
use yii\db\Migration;

class m190802_050553_create_colleges_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('colleges',[
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'status' => "ENUM('active','inactive')",
            'created_by' => Schema::TYPE_INTEGER."  NOT NULL",
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'updated_at' => Schema::TYPE_DATETIME,
        ]);

        $this->createIndex(
            'idx-colleges-created_by',
            'colleges',
            'created_by'
        );


        $this->addForeignKey(
            'fk-colleges-created_by',
            'colleges',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );        
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-colleges-created_by',
            'colleges'
        );

        $this->dropIndex(
            'idx-colleges-created_by',
            'colleges'
        );

        $this->dropTable('colleges');
        
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
