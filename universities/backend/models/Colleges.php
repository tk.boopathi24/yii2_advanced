<?php

namespace backend\models;
use common\models\User;
use Yii;

/**
 * This is the model class for table "colleges".
 *
 * @property integer $id
 * @property string $name
 * @property string $status
 * @property integer $created_by
 *
 * @property User $CreatedBy
 */
class Colleges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'colleges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['status'], 'required'],
            [['created_by'], 'required'],
            [['created_by'], 'integer'],
            [['file'],'file','skipOnEmpty' => true, 'extensions' => 'png, jpg ,jpeg', 'wrongExtension'=>'{extensions} files only', 'on'=>'update'],
            [['file'],'required'],
            [['name','status','logo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            "file"=>Yii::t('app', 'Logo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
         $user = new User();
        return $this->hasOne($user->className(), ['id' => 'created_by']);
    }
}
