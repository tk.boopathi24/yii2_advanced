<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Colleges;
use backend\models\Branches;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\Departments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="departments-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?= $form->field($model, 'college_id')->dropDownList(
        ArrayHelper::map(Colleges::find()->all(),'id','name'),
        ['prompt'=>"select College",
            'onchange' => '$.post("index.php?r=branches/list&id='.'"+$(this).val(),function( data ){
                $("select#departments-branch_id").html(data);
            });'
        ]); ?>
    <?= $form->field($model, 'branch_id')->dropDownList(
        ArrayHelper::map(Branches::find()->all(),'id','name'),['prompt'=>"select Branch"]
    ) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'strength')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Status']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
