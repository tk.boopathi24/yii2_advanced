<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CollegesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Colleges');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="colleges-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button(Yii::t('app', 'Create Colleges'),  [ 'value'=>Url::to('index.php?r=colleges/create') , 'id'=>'modalButton','class' => 'btn btn-success']) ?>
    </p>
    <?php
        Modal::begin([
            'header'=>'<h4> Colleges</h4>',
            'id'=>'modal',
            'size'=>'modal-lg',
        ]);

        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>


    <?php Pjax::begin();?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [   'attribute'=>'logo',
                "format"=>"raw",
                "value"=>function($model){
                    if(isset($model->logo)){
                        return  '<img height="10%" src="'.\Yii::$app->request->BaseUrl.'/'.$model->logo.'">';
                    }
            },
            ],            
            'status',
            'createdBy.username',
          ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 <?php Pjax::end();?>
</div>
