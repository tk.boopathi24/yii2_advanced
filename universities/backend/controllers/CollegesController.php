<?php

namespace backend\controllers;

use Yii;
use backend\models\Colleges;
use backend\models\CollegesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * CollegesController implements the CRUD actions for Colleges model.
 */
class CollegesController extends Controller
{
    public function behaviors()
    {
        return [
            // 'access' =>[
            //     'class'=> AccessControl::className(),
            //     'only'=>['create','update'],
            //     'rules'=>[
            //         'allow'=>true,
            //         'roles'=>['@']
            //     ],
            // ],            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Colleges models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CollegesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Colleges model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Colleges model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Colleges();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->created_at = date('Y-m-d h:i:s');
            $model->created_at = date('Y-m-d h:i:s');
            $model->created_by = Yii::$app->user->id;
            $image_name= $model->name;
            $model->file = UploadedFile::getInstance($model,'file');
                $model->file->saveAs('logos/'.$image_name.'.'.$model->file->extension);
            $model->logo = 'logos/'.$image_name.'.'.$model->file->extension;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Colleges model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            $model->updated_at = date('Y-m-d h:m:s');
            $model->created_by = Yii::$app->user->id;
            $image_name= $model->name;
            $model->file = UploadedFile::getInstance($model,'file');
            $model->file->saveAs('logos/'.$image_name.'.'.$model->file->extension);
            $model->logo = 'logos/'.$image_name.'.'.$model->file->extension;
            $model->save();            
                        
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Colleges model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Colleges model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Colleges the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Colleges::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
